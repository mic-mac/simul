def load_data():
    print(0)

constraints = {
    "monthly_available_energy_drop": 0.1
}

# dataset : https://airtable.com/tblFkG1vsox7VAQ1R/
tutorials = {
    "tutos": [
        {
            "name": "tuto1",
            "stats": [
                {
                    "name": "profile1",
                    "pass": "30%",
                    "mean_duration": "3min",
                    "failing_step": "3",
                    "retention": "90%"
                },
                {
                    "name": "profile2",
                    "pass": "70%",
                    "mean_duration": "13min",
                    "failing_step": "17",
                    "retention": "70%"
                }
            ]
        },
        {
            "name": "tuto2",
            "stats": [{
                "name": "profile1",
                "pass": "75%",
                "mean_duration": "5min",
                "failing_step": "9",
                "retention": "90%"
            },
            {
                "name": "profile2",
                "pass": "20%",
                "mean_duration": "7min",
                "failing_step": "2",
                "retention": "20%"
            }
            ]
        },
    ]
}

print("il reste à simuler…")

def simulation(case):
    if case["name"] == perfect:
        return {
            "graph" : {
                "node1": {
                    "termination_duration": 2
                    },
                "node2": {
                    "termination_duration": 3
                    },
                "edge1": {
                    "time_to_transfer": 1
                    }
            },
            "observables": {
                "time_to_translate_totally": 6
            }
        }
    elif case["name"] == "nothing changed":
        return {
            "graph" : {
                "node1": {
                    "termination_duration": 7
                    },
                "node2": {
                    "termination_duration": 14
                    },
                "edge1": {
                    "time_to_transfer": 5
                    }
            },
            "observables": {
                "time_to_translate_totally": 26
            }
        }
    else:
        print("case not known")

cases = {
    "cases": [
        {
            "name": "perfect",
            "description": "all agents have adopted best tutorials"
        },
        {
            "name": "nothing changed",
            "description": "all actors have low skills"
        }
    ]
}

simulation(cases["cases"][0])

print("il reste à visualiser les effets et à interpréter")


print("il reste à permettre d'utiliser le modèle pour prédire les POINTS de BOUCHON")